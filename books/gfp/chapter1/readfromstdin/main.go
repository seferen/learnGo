package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	defer os.Stdin.Close()
	scan := bufio.NewScanner(os.Stdin)
	fmt.Println("Press control + D on a new line to end this programm")
	for scan.Scan() {
		fmt.Println(">", scan.Text())
	}
}
