package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func main() {

	if r1, err := http.Get("http://www.google.com/robots.txt"); err == nil {
		log.Panicln(err)
		defer r1.Body.Close()
		body, err := ioutil.ReadAll(r1.Body)
		if err != nil {
			log.Panicln(err)
		}
		log.Println(string(body))
	}

	if r2, err := http.Head("http://www.google.com/robots.txt"); err == nil {
		defer r2.Body.Close()
	}

	form := url.Values{}
	form.Add("foo", "bar")

	r3, err := http.Post("http://www.google.com/robots.txt",
		"application/x-www-urlencoded",
		strings.NewReader(form.Encode()))
	defer r3.Body.Close()

	r4, err := http.PostForm("http://www.google.com/robots.txt", form)
	defer r4.Body.Close()

	req, err := http.NewRequest(http.MethodDelete,
		"http://www.google.com/robots.txt", nil)

	if err != nil {
		log.Println(err)
	}
	http.DefaultClient.Do(req)

	req, err = http.NewRequest(http.MethodPut,
		"http://www.google.com/robots.txt",
		strings.NewReader(form.Encode()))
}
